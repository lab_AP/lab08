#include "treePrivate.h"

/*
 *  read a file and create tree
 */
node_t *read_file(char *filename)
{
  char name[N], left[N], right[N];
  node_t *root, *father;
  FILE *fp;

  fp = util_fopen(filename, "r");
  root = NULL;
  while (fscanf(fp, "%s %s %s", name, left, right) != EOF) {
    if (root == NULL) {
      root = new_node(name);
    }

    father = search_r(root, name);
    util_check_m(father!=NULL, "wrong file format!\n");

    if (strcmp(left, "-") != 0) {
      father->left = new_node(left);
    }
    if (strcmp(right, "-") != 0) {
      father->right = new_node(right);
    }
  }
  fclose(fp);

  return root;
}

/*
 *  recursively search the node with a given name
 */
node_t *search_r(node_t *root, char *name)
{
  node_t *tmp;

  if (root == NULL) {
    return NULL;
  }
  
  if (strcmp(root->name, name) == 0) {
    return root;
  }

  tmp = search_r(root->left, name);
  if (tmp != NULL) {
    return tmp;
  } 

  return search_r(root->right, name);  
}

/*
 *  recursively search the node with a given name
 */
node_t *new_node(char *name)
{
  node_t *node;

  node = (node_t *)util_malloc(sizeof(node_t));
  node->name = util_strdup(name);
  node->left = node->right = NULL;
  return node;
}

/*
 *  recursively compute the number of nodes in the tree
 */
int count_r(node_t *root)
{
  int count=1;

  if (root == NULL) {
    return 0;
  }

  count += count_r(root->left);
  count += count_r(root->right);
  return count;
}

/*
 *  recursively compute the height of the tree
 */
int height_r(node_t *root)
{
  int hl, hr;

  if (root == NULL) {
    return -1;
  }

  hl = height_r(root->left);
  hr = height_r(root->right);
  return (hl>hr ? hl : hr) + 1;
}

/*
 *  recursively visit the tree
 */
void visit_r(node_t *root, int mode)
{
  if (root == NULL) {
    return;
  }

  if (mode < 0) {
    /* pre-order */
    printf("%s ", root->name);
  }

  visit_r(root->left, mode);

  if (mode == 0) {
    /* in-order */
    printf("%s ", root->name);
  }

  visit_r(root->right, mode);

  if (mode > 0) {
    /* post-order */
    printf("%s ", root->name);
  }
}

/*
 *  recursively deallocate the momeory used for the entire tree
 */
void quit_r(node_t *root)
{
  if (root == NULL) {
    return;
  }

  quit_r(root->left);
  quit_r(root->right);
  free(root->name);
  free(root);
}
