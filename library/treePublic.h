#ifndef TREE_PUBLIC_INCLUDED
#define TREE_PUBLIC_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"

#define N 11

/* structure declaration */
typedef struct node node_t;

/* function prototypes */
node_t *read_file(char *filename);
node_t *search_r(node_t *root, char *name);
node_t *new_node(char *name);
int count_r(node_t *root);
int height_r(node_t *root);
void visit_r(node_t *root, int mode);
void quit_r(node_t *root);

#endif
