#include "treePublic.h"

/*
 *  main program
 */
int main (int argc, char *argv[])
{
  int height=-1, count=0, mode=0;
  node_t *root;

  util_check_m(argc>=2, "missing parameter(s)");

  root = read_file(argv[1]);

  count = count_r(root);
  printf("Number of nodes: %d\n", count);

  height = height_r(root);
  printf("Tree height: %d\n", height);

  if (argc >= 3) {
    sscanf(argv[2], "%d", &mode);
    printf("Visit result ");
    if (mode < 0) {
      printf("(pre-order): ");
    } else if (mode == 0) {
      printf("(in-order): ");
    } else {
      printf("(post-order): ");
    }
    visit_r(root, mode);
    printf("\n");
  }

  quit_r(root);

  return EXIT_SUCCESS;
}
 
