#ifndef TREE_PRIVATE_INCLUDED
#define TREE_PRIVATE_INCLUDED

#include "treePublic.h"

/* structure declaration */
struct node {
  char *name;
  struct node *left;
  struct node *right;
};

#endif
